/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produit_service;

import entites.Produit;
import java.util.List;

/**
 *
 * @author EDOU DODJI
 */
public class ProduitService {
    
    static List<Produit> liste; // à considérer comme la base de données des objets Categorie
    
   public void ajouter(Produit e){
      liste.add(e);
    }; 
    // ajoute l'objet e dans la collection liste
    public void modifier(Produit e){
        
    	for(int i=0;i<liste.size();i++){
    		
    		if(liste.get(i).getId()==e.getId()){
    			liste.get(i).setCategorie(e.getCategorie());
                        liste.get(i).setDatePeremption(e.getDatePeremption()); 
                        liste.get(i).setLibelle(e.getLibelle());
                        liste.get(i).setPrixUnitaire(e.getPrixUnitaire());
    		}
    	}
    	

    }; 
    // remplace par e, l'objet Categorie de la liste qui a même id que e
    public Produit trouver(Integer id){
        
    	for(int i=0;i<liste.size();i++){
    		
    		if(liste.get(i).getId()==id){
    			return liste.get(i);
    		}
    	}
    


    };
    // renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
    public void supprimer(Integer id){
       
    	for(int i=0;i<liste.size();i++){
    		
    		if(liste.get(i).getId()==id){
    			liste.remove(liste.get(i));
    		}
    	}
    	


    }; 
    // retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
    public void supprimer(Produit e){
       
    	for(int i=0;i<liste.size();i++){
    		
    		if(liste.get(i)==e){
    		 liste.remove(e);
    		}
    	}
    	

    };
    // retirer de la liste, l'objet Categorie passé en paramètre
    public List<Produit> lister(){
       return liste;
    }; 
    // renvoyer tous les éléments de la liste
    public List<Produit> lister(int debut, int nombre){
        List<Produit> list; 
        int nbr=0;
           for(int i=debut;i<liste.size();i++){
    		
    		if(nbr < nombre){
                    list.add(liste.get(i));
    			nbr++;
    		}
    	}
    	return list;
    };
    // renvoyer nombre éléments de la liste, commençant à la position debut


    
}
