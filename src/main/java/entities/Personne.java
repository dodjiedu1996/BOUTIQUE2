/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author Dodji EDOU
 */
public class Personne implements java.io.Serializable {
    int   id;
    private String nom;
    private String prenom;
    private LocalDate dateNaissance;

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }
    
    public Personne(int id,String nom,String prenom,LocalDate dateNaissance){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
    }
    
    
    public int getAge(){
        return LocalDate.now().getYear() - this.dateNaissance.getYear();
    }
    
    public int getAge(LocalDate date){
       return this.dateNaissance.getYear()<date.getYear()?date.getYear() - this.dateNaissance.getYear():null;
    }
    
    public int getId(){
        return this.id;
    }
    
    public String getNom(){
        return this.nom;
    }
    
    public String getPrenom(){
        return this.prenom;
    }
    
    public void setId(int Id){
        this.id = id;
    }
    public void setNom(String nom){
        this.nom = nom;
    }
    public void setPrenom(String prenom){
        this.prenom = prenom;
    }
    public void setDateNaissance(LocalDate dateNaissance){
        this.dateNaissance = dateNaissance;
    }
    
    public boolean equals(Personne autrePersonne){
        return this.id==autrePersonne.id;
    }
    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + this.id;
        hash = 17 * hash + Objects.hashCode(this.nom);
        hash = 17 * hash + Objects.hashCode(this.prenom);
        hash = 17 * hash + Objects.hashCode(this.dateNaissance);
        return hash;
    }

    @Override
    public String toString() {
        return "Personne{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + dateNaissance + '}';
    }
    
    
}
