/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.time.LocalDate;

/**
 *
 * @author Dodji EDOU
 */
public class Produit implements java.io.Serializable {
    private int id;
    private String libelle;
    private int prixUnitaire;
    private LocalDate datePeremption;
    private Categorie categorie=new Categorie();
    
    public Produit(int id,String libelle,int prixUnitaire,LocalDate datePeremption,String categ){
        this.id = id;
        this.prixUnitaire = prixUnitaire;
        this.libelle = libelle;
        this.datePeremption = datePeremption;
        categorie.setLibelle(categ);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(int prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public LocalDate getDatePeremption() {
        return datePeremption;
    }

    public void setDatePeremption(LocalDate datePeremption) {
        this.datePeremption = datePeremption;
    }*
    
    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Categorie getCategorie() {
        return categorie;
    }
    
    
    /* public boolean estPerime(){
        return true?this.datePeremption.before(LocalDate.now()):false;
    }
    
    public boolean estPerime(LocalDate date){
        return true?this.datePeremption.before(date):false;
    
     }*/

}
