/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

/**
 *
 * @author Dodji EDOU
 */
public class ProduitAchete implements java.io.Serializable {
    private Produit produit;
    private int quantite=1;
    private double remise=0;
    private Achat achat;
    private int id;
 
    

    public ProduitAchete(int id,int quantite,double remise,Achat achat,Produit produit) {
        this.quantite = quantite;
        this.remise = remise;
        this.achat=achat;
        this.produit=produit;
        this.id=id;
        
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    public ProduitAchete(int quantite,double remise,Produit produit) {
        this.quantite = quantite;
        this.remise = remise;
        this.produit=produit;
        
    }


    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }
    
    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Achat getAchat() {
        return achat;
    }

    public void setAchat(Achat achat) {
        this.achat = achat;
    }

    
    
    
    public int getPrixTotal(){
        return produit.getPrixUnitaire()*quantite - (int)(produit.getPrixUnitaire()*quantite*remise);
    }
    
    

}
