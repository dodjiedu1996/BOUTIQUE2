/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

/**
 *
 * @author Dodji EDOU
 */
 
import java.time.LocalDate;
import java.io.Serializable;
import java.util.ArrayList;


public class Achat implements java.io.Serializable{
    
    private int id;
    private double remise=0;
    private LocalDate dateAchat;
    private  ArrayList<ProduitAchete> produitsAchetes;
    private  Personne personne;
    
    public Achat(int id,double remise,LocalDate dateAchat,Personne personne,ArrayList<ProduitAchete> produitsAchetes){
        this.id = id;
        this.remise = remise;
        this.dateAchat = dateAchat;
        this.personne=personne;
        this.produitsAchetes=produitsAchetes;
        
    }

    public ArrayList<ProduitAchete> getProduitsAchetes() {
        return produitsAchetes;
    }

    public Personne getPersonne() {
        return personne;
    }
    
    public void setProduitsAchetes(ArrayList<ProduitAchete> produitsAchete) {
        this.produitsAchetes=produitsAchete;
    }

public void setPersonne(Personne p) {
        this.personne=p;
    }

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public LocalDate getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(LocalDate dateAchat) {
        this.dateAchat = dateAchat;
    }
    
    public double getRemiseTotale(){
        long rem = 0;
        for(int j=0;j<produitsAchetes.size();j++){
          rem+=(long) (produitsAchetes.get(j).getQuantite()*produitsAchetes.get(j).getProduit().getPrixUnitaire()*produitsAchetes.get(j).getRemise());
        }
        return rem;
    }
    
    public double getPrixTotal(){
        long prix = 0;
        for(int j=0;j<produitsAchetes.size();j++){
          prix+=(long) (produitsAchetes.get(j).getQuantite()*produitsAchetes.get(j).getProduit().getPrixUnitaire());
        }
        return  (prix-getRemiseTotale());
    }
   

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.id;
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.remise) ^ (Double.doubleToLongBits(this.remise) >>> 32));
        hash = 59 * hash + Objects.hashCode(this.dateAchat);
        return hash;
    }

    public boolean equals(Achat autreAchat) {
        return this.id == autreAchat.id;
    }

    @Override
    public String toString() {
        return "Achat{" + "id=" + id + ", remise=" + remise + ", dateAchat=" + dateAchat + ", nomEmp=" + nomEmp + ", produitsAchetes=" + produitsAchetes + ", personne=" + personne + '}';
    }

      
    
    
    
    
}
