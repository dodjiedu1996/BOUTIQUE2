/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Dodji EDOU
 */
public class Client extends Personne implements java.io.Serializable {
    private String carteVisa;
     private ArrayList<Achat> achat;

      
   
    public Client(int id,String nom,String prenom,LocalDate dateNaissance,String carteVisa){
        super(id,nom,prenom,dateNaissance);
        this.carteVisa=carteVisa;
    }
    
    public Client(int id,String nom,String prenom,LocalDate dateNaissance,String carteVisa,ArrayList<Achat> achat){
        super(id,nom,prenom,dateNaissance);
        this.carteVisa=carteVisa;
        this.achat=achat;
    }


    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.carteVisa);
        return hash;
    }

    public boolean equals(Client autreClient) {
        return this.getId()==autreClient.getId();
    }
    
    public void setCarteVIsa(String carteVisa){
        this.carteVisa = carteVisa;
    }
    public String getCarteVisa(){
        return this.carteVisa;
    }
    
     public void setAchat(ArrayList<Achat> achat) {
        this.achat = achat;
    }

    public ArrayList<Achat> getAchat() {
        return achat;
    }

        
     /*public String toString() {
        return "Client:\t" +
                "Identifiant :" + id +
                "\tNom : " + nom + 
                "\tprenom : " + prenom+ 
                 "\tdateNaissance: " + dateNaissance +
                 "\tprenom : " + achat.getproduitsAchetes() ;
    }*/

    @Override
    public String toString() {
        return "Client{" + "carteVisa=" + carteVisa + ", achat=" + achat + '}';
    }
    
    
}
