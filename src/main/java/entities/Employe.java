/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Dodji EDOU
 */
public class Employe extends Personne implements java.io.Serializable {
     private String cnss;
    private LocalDate dateEmbauche;
    private ArrayList<Achat> achat;

    
    public Employe(int id,String nom,String prenom,LocalDate dateNaissance,String cnss,LocalDate dateEmbauche){
        super(id,nom,prenom,dateNaissance);
        this.cnss=cnss;
        this.dateEmbauche=dateEmbauche;
    }
    
    public Employe(int id,String nom,String prenom,LocalDate dateNaissance,String cnss,LocalDate dateEmbauche,ArrayList<Achat> achat){
        super(id,nom,prenom,dateNaissance);
        this.cnss=cnss;
        this.dateEmbauche=dateEmbauche;
        this.achat=achat;
    }

    public String getCnss() {
        return cnss;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }

    public LocalDate getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(LocalDate dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }
    
    public ArrayList<Achat> getAchat() {
        return achat;
    }

    public void setAchat(ArrayList<Achat> achat) {
        this.achat = achat;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.cnss);
        hash = 83 * hash + Objects.hashCode(this.dateEmbauche);
        return hash;
    }

    public boolean equals(Employe autreEmploye) {
        return this.getId()==autreEmploye.getId();
    }

    @Override
    public String toString() {
        return "Employe{" + "cnss=" + cnss + ", dateEmbauche=" + dateEmbauche + ", achat=" + achat + '}';
    }
    
    
    

}
