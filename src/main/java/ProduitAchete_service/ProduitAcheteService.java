/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProduitAchete_service;

import entites.ProduitAchete;
import java.util.List;

/**
 *
 * @author EDOU DODJI
 */
public class ProduitAcheteService {
    
    static List<ProduitAchete> liste; // à considérer comme la base de données des objets Categorie
    
   public void ajouter(ProduitAchete e){
      liste.add(e);
    }; 
    // ajoute l'objet e dans la collection liste
    public void modifier(ProduitAchete e){
        
    	for(int i=0;i<liste.size();i++){
    		
    		if(liste.get(i).getId()==e.getId()){
    			liste.get(i).setAchat(e.getAchat());
                        liste.get(i).setProduit(e.getProduit());
                        liste.get(i).setQuantite(e.getQuantite());
                        liste.get(i).setRemise(e.getRemise());
    		}
    	}
    	

    }; 
    // remplace par e, l'objet Categorie de la liste qui a même id que e
    public ProduitAchete trouver(Integer id){
        
    	for(int i=0;i<liste.size();i++){
    		
    		if(liste.get(i).getId()==id){
    			return liste.get(i);
    		}
    	}
    


    };
    // renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
    public void supprimer(Integer id){
       
    	for(int i=0;i<liste.size();i++){
    		
    		if(liste.get(i).getId()==id){
    			liste.remove(liste.get(i));
    		}
    	}
    	


    }; 
    // retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
    public void supprimer(ProduitAchete e){
       
    	for(int i=0;i<liste.size();i++){
    		
    		if(liste.get(i)==e){
    		 liste.remove(e);
    		}
    	}
    	

    };
    // retirer de la liste, l'objet Categorie passé en paramètre
    public List<ProduitAchete> lister(){
       return liste;
    }; 
    // renvoyer tous les éléments de la liste
    public List<ProduitAchete> lister(int debut, int nombre){
        List<ProduitAchete> list; 
        int nbr=0;
           for(int i=debut;i<liste.size();i++){
    		
    		if(nbr < nombre){
                    list.add(liste.get(i));
    			nbr++;
    		}
    	}
    	return list;
    };
    // renvoyer nombre éléments de la liste, commençant à la position debut

    
    
}
