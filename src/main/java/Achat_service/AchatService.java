/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Achat_service;

import entites.Achat;
import java.util.List;

/**
 *
 * @author EDOU DODJI
 */
public class AchatService {
    static List<Achat> liste; // à considérer comme la base de données des objets Categorie
    
   public void ajouter(Achat a){
      liste.add(a);
    }; 
    // ajoute l'objet e dans la collection liste
    public void modifier(Achat e){
        
    	for(int i=0;i<liste.size();i++){
    		
    		if(liste.get(i).getId()==e.getId()){
    			liste.get(i).setRemise(e.getRemise());
                        liste.get(i).setDateAchat(e.getDateAchat());
                        liste.get(i).setPersonne(e.getPersonne());
                        liste.get(i).setProduitsAchetes(e.getProduitsAchetes());
    		}
    	}
    	

    }; 
    // remplace par e, l'objet Categorie de la liste qui a même id que e
    public Achat trouver(Integer id){
        
    	for(int i=0;i<liste.size();i++){
    		
    		if(liste.get(i).getId()==id){
    			return liste.get(i);
    		}
    	}
    


    };
    // renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
    public void supprimer(Integer id){
       
    	for(int i=0;i<liste.size();i++){
    		
    		if(liste.get(i).getId()==id){
    			liste.remove(liste.get(i));
    		}
    	}
    	


    }; 
    // retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
    public void supprimer(Achat e){
       
    	for(int i=0;i<liste.size();i++){
    		
    		if(liste.get(i)==e){
    		 liste.remove(e);
    		}
    	}
    	

    };
    // retirer de la liste, l'objet Categorie passé en paramètre
    public List<Achat> lister(){
       return liste;
    }; 
    // renvoyer tous les éléments de la liste
    public List<Achat> lister(int debut, int nombre){
        List<Achat> list; 
        int nbr=0;
           for(int i=debut;i<liste.size();i++){
    		
    		if(nbr < nombre){
                    list.add(liste.get(i));
    			nbr++;
    		}
    	}
    	return list;
    };
    // renvoyer nombre éléments de la liste, commençant à la position debut

}
